# OpenSim - Docker Image

This repository contains the source (Dockerfile) of the OpenSim Docker image.

The main purpose of this image is run other build pipelines.

Find the Docker image at: <https://hub.docker.com/repository/docker/be1et/opensim>
