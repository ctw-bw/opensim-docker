#
# This Dockerfile is going to download, build and install the Opensim dependencies. Then
# it will download the Opensim binaries (so without compiling them itself) and install those.
# The reason is that the dependencies already take almost half an hour, but compiling Opensim
# itself would require way more than an hour. So a lot of build time is saved by getting the 
# binaries instead.
#

FROM ubuntu:20.04

LABEL maintainer="robert.soor@gmail.com"

# Set OpenSim version
ENV VERSION 4.1

# Set directory
ENV OPENSIM_HOME "/opensim_install"

# Get packages
ENV DEBIAN_FRONTEND=noninteractive
RUN apt update -q && apt install -y git build-essential libtool autoconf cmake gfortran wget unzip \
        pkg-config \
        libopenblas-dev \
        liblapack-dev \
        python3 python3-dev python3-numpy python3-matplotlib python3-setuptools \
        swig

# Get opensim source
RUN git clone https://github.com/opensim-org/opensim-core.git && \
    cd opensim-core && git checkout $VERSION

# Build dependencies
RUN mkdir opensim_dependencies_build && cd opensim_dependencies_build && \
    cmake ../opensim-core/dependencies -DCMAKE_INSTALL_PREFIX='/opensim_dependencies_install' \
                                       -DCMAKE_BUILD_TYPE=RelWithDebInfo

RUN cd opensim_dependencies_build && make -j8
# `make -j8` is fast, but could crash the container

# Prepare opensim build
RUN mkdir opensim_build && cd opensim_build && \
    cmake ../opensim-core -DCMAKE_INSTALL_PREFIX="/opensim_install" \
                          -DCMAKE_BUILD_TYPE=RelWithDebInfo \
                          -DOPENSIM_DEPENDENCIES_DIR="/opensim_dependencies_install" \
                          -DBUILD_PYTHON_WRAPPING=OFF

# Do OpenSim build
RUN cd opensim_build && make -j8

# Perform OpenSim install
RUN cd opensim_build && make install -j8

ENTRYPOINT ["/bin/bash"]
